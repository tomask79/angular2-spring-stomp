# Writing applications in Angular 2 [part 19] #

## Angular2 STOMP protocol client and Spring STOMP backend ##

Last time in [part 18](https://bitbucket.org/tomask79/angular2-sockjs-spring) we had **SockJS** application which helps with solving problem of WebSocket compatibility. In [part 16](https://bitbucket.org/tomask79/angular2-websockets-spring) I showed how to create pure **WebSocket** application with Angular2. But, there is still something missing. Mentioned solutions are not much enterprise. How about:

* **Subscribe** all web clients on events on particular **JMS queue/topic**.
* **Broadcast** through **WebSocket/SockJS** message to all subscribed clients when particular event occurs on JMS artifact.
* Use various brokers like ActiveMQ, RabbitMQ which supports such protocol.

Basically, I just described you a [STOMP protocol](https://stomp.github.io/). Definitely check it out. Also check out how you can communicate with STOMP message broker over [Spring](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html#websocket-stomp-overview).

To understand how STOMP works, let's build a demo based on Angular2 and Spring...First let's configure Spring STOMP endpoint.

```
@Configuration
@EnableWebSocketMessageBroker
public class ContextRootConfig extends AbstractWebSocketMessageBrokerConfigurer {
	
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/stompTest");
    }

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry config) {
        config.setApplicationDestinationPrefixes("/app");
        config.enableSimpleBroker("/topic", "/queue");
    }
}
```
* **'/stompTest'** is going to be endpoint address we will connect to through WebSocket. You can also use [SockJS](https://github.com/sockjs/sockjs-protocol).
* **config.setApplicationDestinationPrefixes("/app")** says that all HTTP calls which begins with '/app' will be handled through [Spring REST](https://spring.io/guides/gs/rest-service/) and routed to controllers method.
* **config.enableSimpleBroker("/topic", "/queue")** enables a simple in-memory broker with topic and queue. You can also use another broker like [ActiveMQ](http://activemq.apache.org/) or [RabbitMQ](https://www.rabbitmq.com/). In that case Spring holds TCP connection to that broker, passes messages to it or vice versa and sends messages to subscribed clients over WebSocket or SockJS.

App controller:

```
@RestController
public class TestController {
		
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public OutputObject greeting(InputObject input) throws Exception {
        Thread.sleep(1000); // simulated delay
        final OutputObject output = new OutputObject();
        output.setOutputField("[Pushed by Spring Framework] Nice, I received "+input.getInputField());
        return output;
    }	
}
```
* Method **greeting** is application entry point, it has to be invoked by URL beginning with '/app'. Remember method **setApplicationDestinationPrefixes**? 
* Now output from method greeting is being **broadcasted to all subscribed clients on topic through WebSocket or SockjS**. Lovely, isn't it?
* We will be sending JSON from client to backend. So don't forget to add JACKSON to maven.


```
		<!--     https://mvnrepository.com/artifact/org.springframework/spring-web -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>${jackson.version}</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson.version}</version>
		</dependency>	
```

Now Angular 2 client. First, STOMP based Angular2 service powered by Reactive [RXJS](https://github.com/Reactive-Extensions/RxJS).

```
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

import 'stompjs';

declare let Stomp:any;

@Injectable()
export class StompService {

    private _stompClient;
    private _stompSubject : Subject<any> = new Subject<any>();

    public connect(_webSocketUrl: string) : void {
        let self = this;
        let webSocket = new WebSocket(_webSocketUrl);
        this._stompClient = Stomp.over(webSocket);
        this._stompClient.connect({}, function (frame) {
            self._stompClient.subscribe('/topic/greetings', function (stompResponse) {
                // stompResponse = {command, headers, body with JSON 
                // reflecting the object returned by Spring framework}
                self._stompSubject.next(JSON.parse(stompResponse.body));
            });
        });
    }

    public send(_payload: string) {
        this._stompClient.send("/app/hello", {}, JSON.stringify({'inputField': _payload}));
    }

    public getObservable() : Observable<any> {
        return this._stompSubject.asObservable();
    }
}
```
Everything here is self explanatory I'd say. Just let's mention that 

```
self._stompClient.subscribe('/topic/greetings'...
```
is a STOMP subscription on what greeting method will push into JMS topic/greeting. Now component which uses mentioned service:


```
import { Component, OnInit } from '@angular/core';
import { StompService } from './stomp.service';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  providers: [StompService]
})
export class AppComponent implements OnInit {
  public inputField = '<enter some text>!';
  public serverResponse: string;

  constructor(private _stompService: StompService) {
  }

  public ngOnInit(): void {
    this._stompService.connect('ws://localhost:8080/stompTest');
    this._stompService.getObservable().subscribe(payload => {
      this.serverResponse = payload.outputField;
    });
  }

  public send(): void {
    this._stompService.send(this.inputField);
  }
}
```

For building the Angular2 STOMP client I used again [Angular-CLI](https://cli.angular.io/) which still runs on Angular2 RC4 by default. I hope they'll support at least RC5 soon, so we could out of the box use it in CLI as well and benefit from things like NgModules.

## Testing the demo ##

* mvn clean install (in the root directory with pom.xml)
* mvn tomcat7:run-war-only
* In the two opened browsers/tabs hit http://localhost:8080 and play with the FORM and see the server response field how broadcast works with STOMP.

regards

Tomas.