import { StompClientPage } from './app.po';

describe('stomp-client App', function() {
  let page: StompClientPage;

  beforeEach(() => {
    page = new StompClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
