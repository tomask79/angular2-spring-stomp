package com.stomp.angular2;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

/**
 */
@RestController
public class TestController {
		
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public OutputObject greeting(InputObject input) throws Exception {
        Thread.sleep(1000); // simulated delay
        final OutputObject output = new OutputObject();
        output.setOutputField("[Pushed by Spring Framework] Nice, I received "+input.getInputField());
        return output;
    }	
}
