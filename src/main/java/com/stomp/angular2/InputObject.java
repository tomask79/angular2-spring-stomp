package com.stomp.angular2;

public class InputObject {

	private String inputField;

	public String getInputField() {
		return inputField;
	}

	public void setInputField(String inputField) {
		this.inputField = inputField;
	}
}
