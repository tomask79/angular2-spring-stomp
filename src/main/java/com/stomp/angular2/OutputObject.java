package com.stomp.angular2;

public class OutputObject {
	public String outputField;

	public String getOutputField() {
		return outputField;
	}

	public void setOutputField(String outputField) {
		this.outputField = outputField;
	}	
}
